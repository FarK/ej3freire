#include "tasks/green_led.h"

#include "gpio.h"

//Esta tarea parpadea el led verde
void GreenLedBlink( void *pvParameters )
{
    //La tarea debe tener un bucle infinito...
    for( ;; )
    {
        //Parpadea el número de veces especificado por el numero
        vTaskDelay(0.2*configTICK_RATE_HZ);
        GPIOPinWrite(GPIOA1_BASE,GPIO_PIN_3,GPIO_PIN_3);
        vTaskDelay(0.2*configTICK_RATE_HZ);
        GPIOPinWrite(GPIOA1_BASE,GPIO_PIN_3,0);
    }
}
