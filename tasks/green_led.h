#ifndef _GREEN_LED_H_
#define _GREEN_LED_H_

#include "FreeRTOS.h"
#include "task.h"

void GreenLedBlink( void *pvParameters );

#endif
