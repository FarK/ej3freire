#ifndef _ORANGE_LED_H_
#define _ORANGE_LED_H_

#include "FreeRTOS.h"
#include "task.h"

void LedOrangeBlink( void *pvParameters );

#endif
