#include "tasks/orange_led.h"
#include "commands.h"

#include "uart.h"

#include "buttons.h"

void LedOrangeBlink( void *pvParameters )
{
    struct LED_cmd LED_cmd;
    unsigned int i;

    //La tarea debe tener un bucle infinito...
    for( ;; )
    {
        //Lee un numero de la cola de mensajes
        xQueueReceive(led_queue,&LED_cmd,portMAX_DELAY);
        UARTprintf("\nParpadeo el LED %s %d veces con un semiperiodo de %d ticks\n",LED_cmd.LED, LED_cmd.times, LED_cmd.ticks);
        UARTprintf("> ");

        for(i=0; i<LED_cmd.times ;i++)
        {
            if (0==strncmp(LED_cmd.LED, "orange",6))
            {
                vTaskDelay(LED_cmd.ticks);
                GPIOPinWrite(GPIOA1_BASE,GPIO_PIN_2,GPIO_PIN_2);
                vTaskDelay(LED_cmd.ticks);
                GPIOPinWrite(GPIOA1_BASE,GPIO_PIN_2,0);
            }
        }
    }
}
