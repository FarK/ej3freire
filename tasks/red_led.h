#ifndef _RED_LED_H_
#define _RED_LED_H_

#include "FreeRTOS.h"
#include "task.h"

void LedRedBlink( void *pvParameters );

#endif
