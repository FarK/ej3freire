/*
 * commands.h
 *
 *  Created on: 2 dic. 2018
 *      Author: FREIRE
 */

#ifndef COMMANDS_H_
#define COMMANDS_H_

struct LED_cmd
 {
    char LED[64];
    int times;
    int ticks;
 };

#endif /* COMMANDS_H_ */
