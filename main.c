//*****************************************************************************
// Copyright (C) 2014 Texas Instruments Incorporated
//
// All rights reserved. Property of Texas Instruments Incorporated.
// Restricted rights to use, duplicate or disclose this code are
// granted through contract.
// The program may not be used without the written permission of
// Texas Instruments Incorporated or against the terms and conditions
// stipulated in the agreement under which this program has been supplied,
// and under no circumstances can it be used with non-TI connectivity device.
//
//*****************************************************************************
// Modificado, adaptado y ampliado por Jose Manuel Cano Garcia y Eva Gonzalez
// Departamento de Tecnologia Electronica
// Universidad de Malaga
//*****************************************************************************
//
//*****************************************************************************

//*****************************************************************************
//
//! \addtogroup mqtt_client
//! @{
//
//*****************************************************************************

#include<stdint.h>
#include<stdbool.h>

// Standard includes
#include <stdlib.h>
#include <string.h>

// driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "interrupt.h"
#include "rom_map.h"
#include "prcm.h"
#include "uart.h"
#include "gpio.h"
#include "timer.h"
#include "utils.h"
#include "utils/uartstdio.h"
#include "utils/cpu_usage.h"
// application specific includes
#include "pinmux.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "portmacro.h"
//L34
#include "event_groups.h"
#include "commands.h"

#include "tasks/green_led.h"
#include "tasks/red_led.h"
#include "tasks/orange_led.h"

#include "buttons.h"

//*****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//*****************************************************************************

void BoardInit(void);


//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************

extern void (* const g_pfnVectors[])(void);

uint32_t g_ui32CPUUsage=0;
//static SemaphoreHandle_t semaforo1,semaforo2;

/*-----------------------------------------------------------*/

extern void vUARTTask( void *pvParameters );

//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************



//****************************************************************************

//*****************************************************************************
// FreeRTOS User Hook Functions enabled in FreeRTOSConfig.h
//*****************************************************************************

//*****************************************************************************
//
//! \brief Application defined hook (or callback) function - assert
//!
//! \param[in]  pcFile - Pointer to the File Name
//! \param[in]  ulLine - Line Number
//!
//! \return none
//!
//*****************************************************************************
void
vAssertCalled( const char *pcFile, unsigned long ulLine )
{
    //Handle Assert here
    while(1)
    {
    }
}

//*****************************************************************************
//
//! \brief Application defined idle task hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void
//vApplicationIdleHook( void)
//{
//    //Handle Idle Hook for Profiling, Power Management etc
//  //
//  PRCMSleepEnter();
//
//}

//*****************************************************************************
//
//! \brief Application defined malloc failed hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationMallocFailedHook()
{
    //Handle Memory Allocation Errors
    while(1)
    {
    }
}

//*****************************************************************************
//
//! \brief Application defined stack overflow hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationStackOverflowHook( void *pxTask,
                                   signed char *pcTaskName)
{
    //Handle FreeRTOS Stack Overflow
    while(1)
    {
    }
}
//-------------- Fin ganchos -------------



//*****************************************************************************
//
//! Board Initialization & Configuration
//!
//! \param  None
//!
//! \return None
//
//*****************************************************************************
void BoardInit(void)
{

    //
    // Set vector table base
    //

    IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);

    //
    // Enable Processor
    //
    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}
  

/*--------------------- Tareas --------------------------------------*/


//Hay una tarea mas, pero esta implementada en commands.c...

/*--------------------- Fin Tareas --------------------------------------*/


//*****************************************************************************
//
//! Main 
//!
//! \param  none
//!
//! This function
//!    1. Invokes the SLHost task (tarea asociada a SimpleLink)
//!    2. Invokes the TCPServer task
//!
//! \return None
//!
//*****************************************************************************
void main()
{
    //
    // Initialize the board configurations
    //
    BoardInit();

    //
    // Pinmux for LED, Buttons and UART
    //
    PinMuxConfig();

    //All LEDs off
    GPIOPinWrite(GPIOA1_BASE,GPIO_PIN_3|GPIO_PIN_2|GPIO_PIN_1,0);

     //
     // Configuring UART
     //
     UARTStdioConfig(0,115200);

     ButtonsInit(); //configure the buttons

     CPUUsageInit(configTICK_RATE_HZ/10, 3); //Cuidado que solo tiene 4 timers!!!


    /* luego creamos las tareas */
     if (xTaskCreate( GreenLedBlink, "GBli", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL )!=pdPASS)
         while (1);
     if (xTaskCreate( LedRedBlink, "LRBli", configMINIMAL_STACK_SIZE+30, NULL, tskIDLE_PRIORITY+1, NULL )!=pdPASS)
         while (1);
     if (xTaskCreate( LedOrangeBlink, "LOBli", configMINIMAL_STACK_SIZE+30, NULL, tskIDLE_PRIORITY+1, NULL )!=pdPASS)
         while (1);

     /* Crea la tarea que gestiona los comandos */
     if (xTaskCreate( vUARTTask, "command", 512, NULL, tskIDLE_PRIORITY+1, NULL )!=pdPASS)
         while (1);

    //
    // Start the task scheduler
    //
    vTaskStartScheduler();

}


