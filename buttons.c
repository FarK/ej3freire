#include "buttons.h"

#include "commands.h"

#include "gpio.h"
#include "hw_ints.h"

#define POSICIONES_COLA 5

// Definición de la cola (reserva de memoria)
QueueHandle_t led_queue;

// Estas funciones son "static", así que no se van a ver ni usar fuera
// de "buttons.h"
static void Button1_ISR (void);
static void Button2_ISR (void);

//Initialze Buttons and ISR
void ButtonsInit(void)
{
    //Los puertos GPIO1 y 2 ya han sido habilitados en la función PinMuxConfig, por lo que no tengo que hacerlo aqui
    // Aqui activamos las interrupciones e instalamos los manejadores de interrupción
    //
    GPIOIntTypeSet(GPIOA1_BASE,GPIO_PIN_5,GPIO_FALLING_EDGE); //Configura flanco de bajada en el PIN5
    IntPrioritySet(INT_GPIOA1, configMAX_SYSCALL_INTERRUPT_PRIORITY); //Configura la prioridad de la ISR
    GPIOIntRegister(GPIOA1_BASE, Button1_ISR); //Registra Button1_ISR como rutina de interrupción del puerto
    GPIOIntClear(GPIOA1_BASE,GPIO_PIN_5); //Borra el flag de interrupcion
    GPIOIntEnable(GPIOA1_BASE,GPIO_INT_PIN_5); //Habilita la interrupcion del PIN
    IntEnable(INT_GPIOA1); //Habilita la interrupcion del puerto

    //Igual configuracion para el otro boton (PIN6 del puerto 2).
    GPIOIntTypeSet(GPIOA2_BASE,GPIO_PIN_6,GPIO_FALLING_EDGE);
    IntPrioritySet(INT_GPIOA2, configMAX_SYSCALL_INTERRUPT_PRIORITY);
    GPIOIntRegister(GPIOA2_BASE, Button2_ISR);
    GPIOIntClear(GPIOA2_BASE,GPIO_PIN_6);
    GPIOIntEnable(GPIOA2_BASE,GPIO_INT_PIN_6);
    IntEnable(INT_GPIOA2);

    //Crea una cola de tamaño X posiciones de tipo entero, esta definido al principio del codigo
    led_queue = xQueueCreate(POSICIONES_COLA,sizeof(struct LED_cmd));
    if (led_queue ==NULL)
    {
        while(1);
    }
}

//------------------------------------ Rutinas de interrupcion -----------------------
// ------------------------------------------------------------ -----------------------
static void Button1_ISR (void)
{   //ISR boton izquierdo (SW3)
    struct LED_cmd LED_cmd;

    uint32_t status;
    BaseType_t xHigherPriorityTaskWoken=pdFALSE;

    UtilsDelay(8000);   //antirrebote Sw "cutre", gasta tiempo de CPU ya que las interrupciones tienen prioridad sobre las tareas
                            //Por simplicidad lo dejamos así...
    status =  GPIOIntStatus(GPIOA1_BASE,1);

    strcpy(LED_cmd.LED, "orange");
    LED_cmd.times = 5;
    LED_cmd.ticks = 500;

    if (status&GPIO_PIN_5)
    {
        xQueueSendFromISR(led_queue,&LED_cmd,xHigherPriorityTaskWoken);
    }
    GPIOIntClear(GPIOA1_BASE,status);
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

static void Button2_ISR (void)
{   //ISR boton derecho (SW2)
    struct LED_cmd LED_cmd;

    uint32_t status;
    BaseType_t xHigherPriorityTaskWoken=pdFALSE;

    UtilsDelay(8000);   //antirrebote Sw "cutre", gasta tiempo de CPU ya que las interrupciones tienen prioridad sobre las tareas
    //Por simplicidad lo dejamos así...

    status =  GPIOIntStatus(GPIOA2_BASE,1);

    strcpy(LED_cmd.LED, "red");
    LED_cmd.times = 5;
    LED_cmd.ticks = 500;

    if (status&GPIO_PIN_6)
    {
        xQueueSendFromISR(led_queue,&LED_cmd,xHigherPriorityTaskWoken);
    }
    GPIOIntClear(GPIOA2_BASE,status);
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
// -------------------- FIN ISR ---------------------------------------- -----------------------

